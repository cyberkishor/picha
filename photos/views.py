from django.views.generic.list import ListView

from photos.models import Photo
from feedback.forms import FeedbackForm
from photos.utils import save_latest_flickr_image

class PhotoView(ListView):
    model = Photo
    template_name = 'photos/photo_list.html'
    paginate_by = 24

    def get_context_data(self, **kwargs):
    	# save_latest_flickr_image()
        context = super(PhotoView, self).get_context_data(**kwargs)
        context['form'] = FeedbackForm()
        return context
